# YouTubeCgi

YouTubeCgi is a simple tool to download videos from YouTube. It is able to fetch normal videos and the protected stuff by doing some Javascript magic.

## Requirements

- python 3
- nodejs 4+

## CLI Usage

```
./youtube.cgi 'https://www.youtube.com/watch?v=QH2-TGUlwu4'
```

## CGI Usage

You must install the CGI script on your web server. Ensure that you have the dependencies set up correctly and that the httpd/www-data user can access them. It may help to switch to that user and try to run the script in CLI mode.

You can then visit the CGI script and paste in a URL to download videos. Or you can use a bookmarklet:

```
javascript:(function(){window.location='https://example.com/path/to/youtube.cgi?url='+escape(location);})()
```

## Bug reports

Please raise issues on the [Bitbucket project](https://bitbucket.org/delx/youtube-cgi/issues?status=new&status=open).
